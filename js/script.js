'use strict';
let page=0;
let limit=10;
let mapg;
let markers=[]
let estacionesg={};
let estaciones={};
let valencia={
    lat: 39.4713939,
    lng: -0.3786479
}
let app=$("#app");
let pagination=$("#pagination")
let mapa=$("#mapacontainer")
let acercade=$("#acercade")
let menu = $("#navbarToggleExternalContent")

//FUNCION DE INICIO
$(function(){
    paginacion();
    mapa.hide();
    acercade.hide();
    pagination.show();
    app.show();
    $.ajax({
        dataType:"json",
        url:'http://mapas.valencia.es/lanzadera/opendata/Valenbisi/JSON',
        success:function(res){
            //Generamos el HTML de la tabla y generamos el mapa con los marcadores
            estacionesg=res.features;
            cambiarPagina();
            putCoordinates();
            //Listeners para cambiar el contenido con el menu
            $("#home").click(mostrarTabla);
            document.getElementById('home').addEventListener('click', mostrarTabla)
            $("#vermapa").click(mostrarMapa);
            $("#acerca").click(mostrarAcerca);
            $("#btnant").click({opcion:-1},cambiarPagina);
            $("#btnsig").click({opcion:1},cambiarPagina);
        }
    })
});



//Funciones
function llenarTabla(){
    //Dejo el contenedor de la tabla vacío
    app.html("");
    //Relleno la tabla con los datos
    let html = "<img src='https://www.vectorlogo.es/wp-content/uploads/2017/07/logo-vector-valenbisi.jpg' alt='' class='img-fluid'>"
    html+="<div class='row'><div class='col-md align-middle'>"
    html+="<table class='table table-hover'><tr class='bg-success text-white'><td>Estacion abierta</td></tr><tr class='bg-danger text-white'><td>Estacion cerrada</td></tr></table></div>"
    html+="<div class='col-md'><table class='table table-hover'>"
    html+="<tr><td><i class='fas fa-credit-card'></i></td><td>Se puede pagar con tarjeta</td></tr>";
    html+="<tr><td><i class='fas fa-bicycle '></i></td><td>No hay bicicletas</td></tr>";
    html+="<tr><td><i class='fas fa-ban'></i></td><td>No hay anclajes</td></tr>";
    html+="</table></div></div>"
    html+="<table class='table table-responsive table-hover'><tr><th>Estación</th><th>Bicis Disponibles</th><th>Espacios disponibles</th><th>Opciones</th><th>Total</th></tr>"
    for (const estacion of estaciones) {
        //Rojo->No tienen bicis
        //Amarillo->No tienen anclajes
        //Verde->Bicis y anclajes
        if (estacion.properties.open=="F") {
            html+="<tr class='bg-danger text-white'>"
        }else{
            html+="<tr class='bg-success text-white'>";
        }
        html+=`<td>${estacion.properties.address} Num. ${estacion.properties.number}</td>`
        html+=`<td>${estacion.properties.available}</td>`;
        html+=`<td>${estacion.properties.free}</td>`;
        
        html+=`<td>`
        if (estacion.properties.ticket=="T") {
            html+=" <i class='fas fa-credit-card text-warning'></i> "
        }
        if (estacion.properties.available==0) {
            html+=" <i class='fas fa-bicycle text-warning'></i> "
        }
        if (estacion.properties.free==0) {
            html+=" <i class='fas fa-ban text-warning'></i> "
        }
        html+=`</td><td>${estacion.properties.total}</td>`;
        html+="</tr>";
    }
    html+="</table>";
    app.html(html);
}

function initMaps() {
    mapg = new google.maps.Map(document.getElementById('mapa'), {zoom: 12, center: valencia,clickableIcons: false});
    
}

function putCoordinates() {
    for (const estacion of estacionesg) {
        //Declaración de la variable que contendrá la URL del icono correspondiente
        let icon;
        //Coloco las coordenadas en esta variable para realizar la conversión
        let coord = {
            x:estacion.geometry.coordinates[0],
            y:estacion.geometry.coordinates[1]
        }
        //Declaro los estandares a convertir, UTM->Origen, wgs84->Para google maps
        let utm="+proj=utm +zone=30";
        let wgs84="+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
        //Realizo la conversión del formato
        coord=(proj4(utm,wgs84,coord));
        //Creo una variable entendible para que googlemaps entienda las coordenadas
        let pos = new google.maps.LatLng(coord.y,coord.x);
        //HTML que contiene la tarjeta de cada uno de los marcadores cuando pulsas encima
        let info=`<h4 class="text-center">${estacion.properties.address}</h3><div class="text-left"><p>Estacion ${estacion.properties.number}</p><p>Bicis disponibles: ${estacion.properties.available}</p>`;
        info+=`<p>Anclajes libres: ${estacion.properties.free}</p>`;
        info+=`<p>Total: ${estacion.properties.total}</p></div>`
        //Declaración de la tarjeta y le añadimos el HTML como content
        let infowindow=new google.maps.InfoWindow({content:info});
        //Decidimos que icono le vamos a dar el marcador según la disponibilidad de biciletas
        if (estacion.properties.available==0) {
            icon="http://maps.google.com/mapfiles/kml/paddle/red-circle.png"
        }else{
            icon="http://maps.google.com/mapfiles/kml/paddle/grn-circle.png"
        }
        //Declaración del marcador, con la posición, el mapa donde va que he declarado antes
        //El icono y el titulo
        let marker = new google.maps.Marker({
            position:pos,
            map: mapg,
            icon:icon,
            title:`Estacion ${estacion.properties.number}`
        });
        //Le añadimos una escucha al marcador que cuando haga click, se abra la tarjeta
        marker.addListener('click', function () {
            infowindow.open(mapg, marker);
        })
        //Y añadimos el marcador a la Array donde se encuentran todos
        markers.push(marker);
    }
    let markerCluster = new MarkerClusterer(mapg,markers,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'})
}
function paginacion(){
    let btnant=$("#btnant");
    let btnsig=$("#btnsig");
    let numpag=$("#numeropag");
    numpag.text(page+1);
    let cant=estacionesg.length;
    
    if (page==Math.ceil(cant/limit)-1) {
        btnsig.hide();
        btnant.show();
    }else if(page==0){
        btnant.hide();
        btnsig.show();
    }else{
        btnant.show();
        btnsig.show();
    }

}
function cambiarPagina(event) {
    if (!event) {
        page=0;
    } else {
        page+=event.data.opcion;
        $('html, body').animate({
            scrollTop: app.offset().top
        },750)
    }
    console.log(page);
    
    let multiple=page*limit;
    estaciones=estacionesg.slice(multiple, multiple+limit);
    llenarTabla();
    paginacion();
}
function mostrarTabla(){
    menu.removeClass('show');
    mapa.hide();
    acercade.hide();
    app.show();
    pagination.show();
}

function mostrarMapa(){
    menu.removeClass('show');
    app.hide();
    acercade.hide()
    mapa.show();
    pagination.hide();
}

function mostrarAcerca() {
    menu.removeClass('show');
    mapa.hide();
    app.hide();
    acercade.show();
    pagination.hide();
}

